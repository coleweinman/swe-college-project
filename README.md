# College Project

 - Git SHA: ee215a55cb19b9591f009ad56df3d28e12822df7

 - Group Number: 10AM Group 5

 - Team Members: Cole Weinman, Jonathan Zhao, Matthew Kozlowski, Kristina Zhou
 - Phase 1 Lead: Cole Weinman
 - Phase 2 Lead: Matthew Kozlowski
 - Phase 3 Lead: Jonathan Zhao
 - Phase 4 Lead: Kristina Zhou
 - Project Name: UniverCity
 - Link: https://gitlab.com/coleweinman/swe-college-project
 - Project description: **A website that links together information about college campuses, the cities they are in, and the companies that are around the cities. This would be useful for prospective college students who want to find information about colleges that they are considering.**
 
- Links
	- Production Website: https://www.univercity.me/
	- Developement Website: https://develop.univercity.me/
	- Gitlab Pipelines: https://gitlab.com/coleweinman/swe-college-project/-/pipelines

- Completion Times Phase 1
	- Matthew Kozlowski
		- Estimated: 15
		- Actual: 20
	- Cole Weinman
		- Estimated: 15
		- Actual: 20
	- Kristina Zhou
		- Estimated: 8
		- Actual: 10
	- Jonathan Zhao
		- Estimated: 5
		- Actual: 7

- Completion Times Phase 2
	- Matthew Kozlowski
		- Estimated: 35
		- Actual: 40
	- Cole Weinman
		- Estimated: 35
		- Actual: 45
	- Kristina Zhou
		- Estimated: 20
		- Actual: 23
	- Jonathan Zhao
		- Estimated: 20
		- Actual: 20

- Completion Times Phase 3
	- Matthew Kozlowski
		- Estimated: 30
		- Actual: 35
	- Cole Weinman
		- Estimated: 30
		- Actual: 40
	- Kristina Zhou
		- Estimated: 20
		- Actual: 21
	- Jonathan Zhao
		- Estimated: 15
		- Actual: 20

- Completion Times Phase 4
	- Matthew Kozlowski
		- Estimated: 15
		- Actual: 12
	- Cole Weinman
		- Estimated: 25 
		- Actual: 18
	- Kristina Zhou
		- Estimated: 15 
		- Actual: 10
	- Jonathan Zhao
		- Estimated: 10
		- Actual: 8

 - URLs of data sources
	 - https://collegescorecard.ed.gov/data/documentation/
	 - https://icon.horse/
	 - https://www.roadgoat.com/business/cities-api
	 - https://bigpicture.io/docs/api/#introduction
	 - https://www.mediawiki.org/wiki/API:Main_page
	 - https://www.microsoft.com/en-us/bing/apis/bing-image-search-api
- Models
	- Colleges
		- Number of instances: ~900
		- Sortable Attributes:
			- Admission Rate
			- Average Cost
			- Enrollment Size
			- SAT Average
			- SAT Median Math Score
			- SAT Median Reading Score
			- In-State Tuition
			- Out-of-State Tuition
		- Searchable Attributes:
			- Name
			- City Name
			- State
			- Type of Control (Private vs Public)
			- Primary Degree Type
		- Additional Attributes:
			- Latitude
			- Longitude
			- Picture
			- Description
	- Cities
		- Number of instances: ~900
		- Sortable Attributes:
			- Area
			- Average Rating
			- Budget
			- Covid Risk
			- Elevation
			- Population
			- Population Density
			- Safety
		- Searchable Attributes:
			- Name
			- Known For
			- Nearby Colleges
			- Nearby Companies
			- State
			- Timezone
		- Additional Attributes
			- Description
			- Picture
			- Latitude
			- Longitude
			- Google Events URL
			- Walk Score URL
	- Companies
		- Number of instances: ~150
		- Sortable Attributes
			- Alexa Global Rank
			- Alexa US Rank
			- Annual Revenue
			- Number of Employees
			- Founded
		- Searchable Attributes
			- Name
			- Legal Name
			- City Name
			- Public vs Private
			- Sector
			- State
			- Technologies
			- Industries
			- Ticker
		- Additional Attributes
			- Domain
			- Description
			- Facebook Handle
			- LinkedIn Handle
			- Twitter Handle
			- Location
			- Logo
			- Homepage URL

- Questions to answer
	- In a given city, what job opportunities might be available from surrounding companies?
	- For a given college, which are most affordable and highly ranked in a given area of study?
	- What kind of activities or opportunities are available at a certain college?

- Comments
	- CSS for styling of splash page banner based on CSS from https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/Splash/Splash.module.css (Lines 3-15)
