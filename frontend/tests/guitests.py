import os

PATH = "./frontend/tests/chromedriver"
website = "https://www.univercity.me"

url = website + "{0}"

if __name__ == "__main__":
    ret = 0
    ret += os.system(
        " ".join(["python3 ./frontend/tests/guitests/home.py", PATH, website])
    )
    ret += os.system(
        " ".join(
            [
                "python3 ./frontend/tests/guitests/companies.py",
                PATH,
                url.format("/companies"),
            ]
        )
    )
    ret += os.system(
        " ".join(
            [
                "python3 ./frontend/tests/guitests/colleges.py",
                PATH,
                url.format("/colleges"),
            ]
        )
    )
    ret += os.system(
        " ".join(
            ["python3 ./frontend/tests/guitests/cities.py", PATH, url.format("/cities")]
        )
    )
    exit(os.WEXITSTATUS(ret))
