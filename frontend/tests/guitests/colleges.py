import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import sys

PATH = sys.argv[1]
website = sys.argv[2]
url = website + "{0}"


class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()), options=chrome_options
        )

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_college_cards(self):
        self.driver.get(website)
        class_name = "MuiButtonBase-root"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_all_elements_located((By.CLASS_NAME, class_name))
        )
        element = self.driver.find_element(by=By.CLASS_NAME, value=class_name)
        element.click()
        regex_pattern = website + "(\d)*"
        self.assertRegex(self.driver.current_url, regex_pattern)

    def test_college_img(self):
        self.driver.get(url.format("/433"))
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_element_located((By.TAG_NAME, "img"))
        )
        img = self.driver.find_element(by=By.TAG_NAME, value="img")
        site = img.get_attribute("src")
        self.assertNotEqual(site, "")

    def test_college_links(self):
        self.driver.get(url.format("/449"))
        css_str = "a[href*='companies']"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_element_located((By.CSS_SELECTOR, css_str))
        )
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_str)
        element.click()
        regex_url = website[0 : website.rfind("/")] + "/companies/"
        regex_pattern = regex_url + "(\d)*"
        self.assertRegex(self.driver.current_url, regex_pattern)

    def test_college_sort(self):
        self.driver.get(website)
        names = ["MuiSelect-select", "a[href*='colleges']"]
        WebDriverWait(self.driver, 10).until(
            ec.presence_of_all_elements_located((By.CLASS_NAME, names[0]))
        )[0].click()
        self.driver.get(url.format("?sortField=tuitionOutOfState&ascending=true"))
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = [
            "Haskell Indian Nations University",
            "Brigham Young University-Idaho",
            "Florida Polytechnic University",
            "Brigham Young University-Hawaii",
            "Brigham Young University",
        ]
        for i in range(5):
            college = (
                rows[i]
                .find_element(by=By.CLASS_NAME, value="MuiTypography-root")
                .text.strip()
            )
            self.assertEqual(
                college, expected[i], msg="This is the name: {0}".format(college)
            )

    def test_college_filter(self):
        self.driver.get(website)
        names = ["MuiBox-root", "a[href*='colleges'"]
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_element_located((By.CLASS_NAME, names[0]))
        ).click()
        self.driver.get(
            url.format(
                "?sortField=admissionRate&ascending=true&stateFilter=MA&typeControlFilter=private+non-profit"
            )
        )
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = [
            "Harvard University",
            "Massachusetts Institute of Technology",
            "Amherst College",
        ]
        for i in range(3):
            college = (
                rows[i]
                .find_element(by=By.CLASS_NAME, value="MuiTypography-root")
                .text.strip()
            )
            self.assertEqual(
                college, expected[i], msg="This is the name: {0}".format(college)
            )

if __name__ == "__main__":
    unittest.main(argv=["first-arg-is-ignored"], exit=True)
