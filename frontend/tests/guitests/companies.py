import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import sys
import time

PATH = sys.argv[1]
website = sys.argv[2]
url = website + "{0}"


class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()), options=chrome_options
        )

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_cities_link(self):
        self.driver.get(url.format("/155"))
        css_str = "a[href*='cities']"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_element_located((By.CSS_SELECTOR, css_str))
        )
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_str)
        element.click()
        regex_url = website[0 : website.rfind("/")] + "/cities/"
        regex_pattern = regex_url + "(\d)*"
        self.assertRegex(self.driver.current_url, regex_pattern)

    def test_twitter_embed(self):
        self.driver.get(url.format("/60"))
        css_str = "a[href*='twitter']"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_element_located((By.CSS_SELECTOR, css_str))
        )
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_str)
        element.click()
        pattern = "https://twitter"
        chwd = self.driver.window_handles
        self.driver.switch_to.window(chwd[1])
        self.assertNotEqual(
            self.driver.current_url.find(pattern),
            -1,
            msg="This is the current page: {0}".format(self.driver.current_url),
        )

    def test_company_sort(self):
        self.driver.get(website)
        names = ["MuiSelect-select", "a[href*='companies']"]
        WebDriverWait(self.driver, 10).until(
            ec.presence_of_all_elements_located((By.CLASS_NAME, names[0]))
        )[0].click()
        self.driver.get(url.format("?sortField=name&ascending=true"))
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = [
            "Abbott Laboratories",
            "AbbVie",
            "ABM Industries",
            "Adobe",
            "Advance Auto Parts",
        ]
        for i in range(5):
            company = (
                rows[i]
                .find_elements(by=By.CLASS_NAME, value="MuiTableCell-root")[0]
                .text.strip()
            )
            self.assertEqual(
                company, expected[i], msg="This is the name: {0}".format(company)
            )

    def test_company_filter(self):
        self.driver.get(website)
        names = ["MuiBox-root", "a[href*='companies'"]
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_element_located((By.CLASS_NAME, names[0]))
        ).click()
        self.driver.get(
            url.format(
                "?sortField=alexaUsRank&ascending=true&annualRevenueMin=9842740349&employeesMin=992&employeesMax=10199"
            )
        )
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = [
            "Netflix",
            "Lennar",
            "Discovery",
            "Federal National Mortgage Association",
            "KKR & Co.",
        ]
        for i in range(5):
            company = (
                rows[i]
                .find_elements(by=By.CLASS_NAME, value="MuiTableCell-root")[0]
                .text.strip()
            )
            self.assertEqual(
                company, expected[i], msg="This is the name: {0}".format(company)
            )

if __name__ == "__main__":
    unittest.main(argv=["first-arg-is-ignored"], exit=True)
