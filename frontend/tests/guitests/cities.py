import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import sys

PATH = sys.argv[1]
website = sys.argv[2]
url = website + "{0}"


class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()), options=chrome_options
        )

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_maps(self):
        self.driver.get(url.format("/1"))
        tag_name = "iframe"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_all_elements_located((By.TAG_NAME, tag_name))
        )
        # make sure there is a map
        text = self.driver.find_element(by=By.TAG_NAME, value=tag_name).get_attribute(
            "title"
        )
        self.assertEqual(text, "map_frame", msg="this is the text:{0}".format(text))

    def test_walk_scores(self):
        self.driver.get(url.format("/740"))
        css_str = "a[href*='walkscore']"
        WebDriverWait(self.driver, 4).until(
            ec.presence_of_element_located((By.CSS_SELECTOR, css_str))
        )
        element = self.driver.find_element(by=By.CSS_SELECTOR, value=css_str)
        element.click()
        pattern = "https://www.walkscore"
        chwd = self.driver.window_handles
        self.driver.switch_to.window(chwd[1])
        self.assertNotEqual(
            self.driver.current_url.find(pattern),
            -1,
            msg="This is the current page: {0}".format(self.driver.current_url),
        )

    def test_city_sort(self):
        self.driver.get(website)
        names = ["MuiSelect-select", "a[href*='cities']"]
        WebDriverWait(self.driver, 10).until(
            ec.presence_of_all_elements_located((By.CLASS_NAME, names[0]))
        )[0].click()
        self.driver.get(url.format("?sortField=area&ascending=false"))
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = ["Long Island", "Jacksonville", "Butte", "Houston", "Oklahoma City"]
        for i in range(5):
            city = (
                rows[i]
                .find_element(by=By.CLASS_NAME, value="MuiTypography-root")
                .text.strip()
            )
            self.assertEqual(
                city, expected[i], msg="This is the name: {0}".format(city)
            )

    def test_city_filter(self):
        self.driver.get(website)
        names = ["MuiBox-root", "a[href*='cities'"]
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_element_located((By.CLASS_NAME, names[0]))
        ).click()
        self.driver.get(
            url.format(
                "?sortField=population&ascending=true&safetyMax=4&safetyMin=2&populationMax=10000"
            )
        )
        WebDriverWait(self.driver, 20).until(
            ec.presence_of_all_elements_located((By.CSS_SELECTOR, names[1]))
        )
        rows = self.driver.find_elements(by=By.CSS_SELECTOR, value=names[1])
        expected = ["Donaldson", "Normal", "Newcastle", "Stevenson", "Sweet Briar"]
        for i in range(5):
            city = (
                rows[i]
                .find_element(by=By.CLASS_NAME, value="MuiTypography-root")
                .text.strip()
            )
            self.assertEqual(
                city, expected[i], msg="This is the name: {0}".format(city)
            )

if __name__ == "__main__":
    unittest.main(argv=["first-arg-is-ignored"], exit=True)
