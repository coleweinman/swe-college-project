import React from "react";
import { act } from "react-dom/test-utils";
import enableHooks from "jest-react-hooks-shallow";
import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import App from "../App";
import About from "../components/about/About";
import HomePage from "../components/home/HomePage";
import CollegeModelCard from "../components/college/CollegeModelCard";
import CityModelCard from "../components/city/CityModelCard";
import CompanyModelCard from "../components/company/CompanyModelCard";
import HomeCard from "../components/home/HomeCard";
import CityInstancePage from "../components/city/CityInstancePage";
import CollegeInstancePage from "../components/college/CollegeInstancePage";
import CompanyInstancePage from "../components/company/CompanyInstancePage";
import { ModelToolbar } from "../components/toolbar/ModelToolbar";
import CityModelPage from "../components/city/CityModelPage";
import CompanyModelPage from "../components/company/CompanyModelPage";
import CollegeModelPage from "../components/college/CollegeModelPage";
import { BrowserRouter } from "react-router-dom";
import states from "../components/common/states";

configure({ adapter: new Adapter() });

enableHooks(jest);

describe("Render cards", () => {
  test("CityModelCard", () => {
    var city = {
      id: "1",
      name: "Test",
      description: "Hello World",
      imageUrl:
        "https://www.discounthotels.com/wp-content/uploads/2016/06/cobleskill2.jpg",
      state: "TX",
      population: 0,
      populationDensity: 0.0,
      knownFor: ["Quiet"],
      safety: "4",
      elevation: 100, // In feet
      budget: "5",
      averageRating: 3.5,
      timezone: "UTC-6 (CST)",
      covidScore: "4",
      googleEventsUrl:
        "https://www.google.com/search?q=events+radnor+pa&ibp=htl;events",
      walkScoreUrl: "https://www.walkscore.com/FL/St._Petersburg",
      wikipediaURL: "",
      latitude: 120.0,
      longitude: -80.0,
      nearbyColleges: [],
      nearbyCompanies: [],
    };
    const cityPage = shallow(
      <BrowserRouter>
        <CityModelCard city={city} />
      </BrowserRouter>
    );
    expect(cityPage).toMatchSnapshot();
  });

  test("CollegeModelCard", () => {
    var college = {
      id: "2",
      name: "University of Birmingham",
      cityID: "2",
      cityName: "Birmingham",
      state: "AL",
      typeControl: "typecontrol",
      typeDegree: "typedegree",
      description: "description",
      homeUrl: "https://www.uta.edu/",
      pictureUrl:
        "https://media-exp1.licdn.com/dms/image/C561BAQFYlFdSHxvPYw/company-background_10000/0/1519801174276?e=2147483647&v=beta&t=yZbHm9FHAdpNhHNHLogGl2ochvzTV4aPtAEUGnruTqs",
      thumbnailUrl:
        "https://tse2.mm.bing.net/th?id=OIP.Plqec1ouYgrKEyx4cHGn7gDIDI&pid=Api",
      latitude: 30.2,
      longitude: -80.0,
      religiousAffiliation: "none",
      admissionRate: 27,
      satAvg: 1300,
      satMidMath: 600,
      satMidRead: 600,
      enrollmentSize: 89011,
      avgCost: 5000,
      tuitionInState: 100,
      tuitionOutOfState: 10000,
      testScoreReq: "testscorereq",
      level: "level",
      nearbyCompanyIDs: [],
      distance: 10,
    };
    const collegePage = shallow(
      <BrowserRouter>
        <CollegeModelCard college={college} />
      </BrowserRouter>
    );
    expect(collegePage).toMatchSnapshot();
  });

  test("CompanyModelCard", () => {
    var company = {
      id: "155",
      name: "Kohl's",
      description: "company description",
      cityName: "Menomonee Falls",
      state: "CA",
      cityId: "919",
      url: "http://kohls.com/",
      domain: "kohls.com",
      logoURL: "http://logo.bigpicture.io/logo/kohls.com",
      industries: ["Beauty", "Home Decor", "Jewelry", "Retail"],
      technologies: ["Google analytics", "ruxit"],
      public: true,
      ticker: "KSS",
      employees: 110000,
      alexaUsRank: 232,
      alexaGlobalRank: 994,
      annualRevenue: 19075000320,
      sector: "Customer Services",
      location:
        "N56 W17000 Ridgewood Drive Menomonee Falls, WI 53051 United States of America",
      legalName: null,
      founded: null,
      twitterHandle: "Kohls",
      facebookHandle: "Kohls",
      linkedinHandle: "company/kohl%e2%80%99s",
      nearbyCollegeIDs: [],
      distance: 10,
    };
    const companyPage = shallow(
      <BrowserRouter>
        <CompanyModelCard company={company} />
      </BrowserRouter>
    );
    expect(companyPage).toMatchSnapshot();
  });
});

describe("Render instances", () => {
  test("CityInstancePage", () => {
    const cityPage = shallow(
      <BrowserRouter>
        <CityInstancePage id={1} />
      </BrowserRouter>
    );
    expect(cityPage).toMatchSnapshot();
  });

  test("CollegeInstancePage", () => {
    const collegePage = shallow(
      <BrowserRouter>
        <CollegeInstancePage id={2} />
      </BrowserRouter>
    );
    expect(collegePage).toMatchSnapshot();
  });

  test("CompanyInstancePage", () => {
    const companyPage = shallow(
      <BrowserRouter>
        <CompanyInstancePage id={11} />
      </BrowserRouter>
    );
    expect(companyPage).toMatchSnapshot();
  });
});

describe("Model pages", () => {
  test("Search cities", () => {
    const citiesSearch = shallow(
      <BrowserRouter>
        <CityModelPage />
      </BrowserRouter>
    );
    expect(citiesSearch).toMatchSnapshot();
  });

  test("Search companies", () => {
    const companiesSearch = shallow(
      <BrowserRouter>
        <CompanyModelPage />
      </BrowserRouter>
    );
    expect(companiesSearch).toMatchSnapshot();
  });

  test("Search colleges", () => {
    const collegesSearch = shallow(
      <BrowserRouter>
        <CollegeModelPage />
      </BrowserRouter>
    );
    expect(collegesSearch).toMatchSnapshot();
  });
});

describe("Searching/Filtering", () => {
  test("Search bar", () => {
    const globalSearch = mount(
      <BrowserRouter>
        <ModelToolbar
          exactFilters={[
            {
              label: "State",
              field: "state",
              options: states,
            },
          ]}
          rangeFilters={[]}
          sortOptions={[]}
        />
      </BrowserRouter>
    );
    act(() => {
      globalSearch
        .find("input")
        .simulate("change", { target: { value: "Louisville" } });
    });
    expect(globalSearch.find("input").render().toString()).toContain(
      "Louisville"
    );
  });

  test("Filter", () => {
    const globalSearch = mount(
      <BrowserRouter>
        <ModelToolbar
          exactFilters={[
            {
              label: "State",
              field: "state",
              options: states,
            },
          ]}
          rangeFilters={[]}
          sortOptions={[]}
        />
      </BrowserRouter>
    );
    act(() => {
      globalSearch.find("button").first().simulate("click");
      globalSearch
        .find("div")
        .find("input")
        .simulate("change", { target: { value: "Alabama" } });
    });
    expect(
      globalSearch.find("div").find("input").render().toString()
    ).toContain("Alabama");
  });
});

describe("Render views", () => {
  test("App", () => {
    const appTest = shallow(<App />);
    expect(appTest).toMatchSnapshot();
  });

  test("About", () => {
    const aboutTest = shallow(<About />);
    expect(aboutTest).toMatchSnapshot();
  });

  test("Home", () => {
    const homeTest = shallow(<HomePage />);
    expect(homeTest).toMatchSnapshot();
  });

  test("HomeCard", () => {
    const homecardTest = shallow(
      <HomeCard
        image={
          "https://files.worldwildlife.org/wwfcmsprod/images/Mountains_New_Hero_Image/hero_small/1bs3lrclhi_mountains_hero.jpg"
        }
        title="title"
        link="link"
        description="str"
      />
    );
    expect(homecardTest).toMatchSnapshot();
  });
});
