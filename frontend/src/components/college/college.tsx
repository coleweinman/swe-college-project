interface College {
  id: string;
  name: string;
  cityId: string;
  cityName: string;
  state: string;
  typeControl: string;
  typeDegree: string;
  description: string | null;
  homeUrl: string;
  pictureUrl: string;
  thumbnailUrl: string | null;
  latitude: number;
  longitude: number;
  religiousAffiliation: string | null;
  admissionRate: number;
  satAvg: number;
  satMidMath: number;
  satMidRead: number;
  enrollmentSize: number;
  avgCost: number;
  tuitionInState: number;
  tuitionOutOfState: number;
  testScoreReq: string;
  level: string;
  nearbyCompanyIDs: string[];
  distance: number;
}

export default College;
