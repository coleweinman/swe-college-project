interface Company {
  id: string;
  name: string;
  description: string;
  cityName: string;
  state: string;
  cityId: string;
  url: string;
  domain: string;
  logoURL: string;
  industries: string[];
  technologies: string[];
  public: boolean;
  ticker: string | null;
  employees: number | null;
  alexaUsRank: number | null;
  alexaGlobalRank: number | null;
  annualRevenue: number;
  sector: string;
  location: string;
  legalName: string;
  founded: number | null;
  twitterHandle: string | null;
  facebookHandle: string | null;
  linkedinHandle: string | null;
  nearbyCollegeIDs: string[];
  distance: number;
}

export default Company;
