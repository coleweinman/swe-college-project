import College from "../college/college";
import Company from "../company/company";

interface City {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
  state: string;
  population: number;
  area: number;
  populationDensity: number;
  knownFor: string[];
  safety: number;
  elevation: number; // In feet
  budget: number;
  averageRating: number;
  timezone: string;
  covidScore: string;
  googleEventsUrl: string;
  walkScoreUrl: string;
  wikipediaURL: string;
  latitude: number;
  longitude: number;
  nearbyColleges: College[];
  nearbyCompanies: Company[];
}

export default City;
