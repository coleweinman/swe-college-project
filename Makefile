.DEFAULT_GOAL := all
SHELL         := bash

FRONTEND_DIR = frontend
BACKEND_DIR = backend

all:

install:
	(cd $(FRONTEND_DIR) && npm install)

run-script:
	(cd $(FRONTEND_DIR) && npm run-script build)

start:
	(cd $(FRONTEND_DIR) && npm start)

run:
	$(MAKE) install
	$(MAKE) start

jest-tests:
	(cd $(FRONTEND_DIR) && npm test)

python-tests:
	echo "Running Python backend tests"
	python3 $(BACKEND_DIR)/tests.py

selenium-tests:
	python3 $(FRONTEND_DIR)/tests/guitests.py	
	

