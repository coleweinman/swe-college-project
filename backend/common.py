from pydoc import pager
from models.nearby_college_company import nearby_college_company
from services import db, base_url, upload_url_image
from sqlalchemy.sql import column
from sqlalchemy import and_
from sqlalchemy import or_
import inspect
import re
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy import func


class PaginationInfo:
    def __init__(self, page, page_size):
        self.page = page
        self.page_size = page_size


def parse_models_pagination_params(req_args):
    page = parse_request_param(req_args, "page", int) or 1
    page_size = parse_request_param(req_args, "pageSize", int) or 15
    return PaginationInfo(page, page_size)


# Parses exact filters from query parameters, gets type from field. List types default to str element type.
def parse_exact_filters(req_args, model, exact_filter_fields):
    filters = {}
    inv_name_dict = {v: k for k, v in model.name_dict.items()}
    for field in exact_filter_fields:
        field_name = inv_name_dict[field.name]
        field_type = field.type.python_type
        if field_type is list:
            field_type = str
        result = parse_request_param(
            req_args, field_name + "Filter", field_type, list=True
        )
        if len(result) > 0:
            filters[field] = result
    return filters


# Parses range filters from query parameters, defaults to int type.
def parse_range_filters(req_args, model, range_filter_fields):
    filters = {}
    inv_name_dict = {v: k for k, v in model.name_dict.items()}
    for field in range_filter_fields:
        field_name = inv_name_dict[field.name]
        min = parse_request_param(req_args, field_name + "Min", int)
        max = parse_request_param(req_args, field_name + "Max", int)
        if min is not None or max is not None:
            filters[field] = (min, max)
    return filters


def parse_sort_fields(req_args, model, sort_fields, default_field):
    # Get search field name from request args
    sort_field_n = parse_request_param(req_args, "sortField", str)
    # if sort_field_n is None:
    #     return None

    # Get ascending value from request args
    do_ascending = parse_request_param(req_args, "ascending", str)
    if do_ascending is not None:
        do_ascending = do_ascending.lower() == "true"
    else:
        do_ascending = False


    # Assemble model.field.desc()/asc() request and return it
    chosen_field = default_field
    if sort_field_n is not None:
        inv_name_dict = {v: k for k, v in model.name_dict.items()}
        for field in sort_fields:
            field_name = inv_name_dict[field.name]
            if field_name == sort_field_n:
                chosen_field = field
                break
    if do_ascending:
        return chosen_field.asc()
    else:
        return chosen_field.desc()
    return None


def parse_request_param(req_args, name, type, list=False):
    if name in req_args:
        try:
            if list:
                args = req_args[name].split(",")
            else:
                args = [req_args[name]]
            ret = []
            for arg in args:
                # if type is int:
                #     ret.append(int(arg))
                # elif type is str:
                #     ret.append(str(arg))
                ret.append(type(arg))
            if list:
                return ret
            else:
                return ret[0]
        except:
            raise Exception({name: name + " is invalid"})
    else:
        if list:
            return []
        else:
            return None


def paginate_request(base_query, page, page_size):
    return base_query.paginate(page, page_size, True)


def add_required_filters(base_query, model, required_fields):
    db_fields = [model.name_dict[rf] for rf in required_fields]
    filters = [column(colname).is_not(None) for colname in db_fields]
    return base_query.filter(and_(*filters))


def nearby_query(
    model, pag_info, nearby_model_id_field, nearby_id_field, nearby_id, max_distance
):
    base_query = (
        db.session.query(nearby_college_company, model)
        .filter(nearby_model_id_field == model.id)
        .filter(nearby_id_field == nearby_id)
        .order_by(nearby_college_company.distance)
    )
    if max_distance is not None:
        base_query = base_query.filter(nearby_college_company.distance <= max_distance)
    results = base_query.paginate(pag_info.page, pag_info.page_size, True)
    instances = []
    for nearby_result, model_result in results.items:
        model_instance = model_to_dict(model_result)
        model_instance["distance"] = nearby_result.distance
        instances.append(model_instance)
    return {
        "status": "success",
        "data": {
            "page": pag_info.page,
            "pageSize": pag_info.page_size,
            "results": len(instances),
            "total": results.total,
            "instances": instances,
        },
    }


def get_model_pages(base_query, pag_info):

    # Make SQL request
    try:
        results = base_query.paginate(pag_info.page, pag_info.page_size, True)
        instances = [model_to_dict(i) for i in results.items]
    except Exception as e:
        print(e)
        return {"status": "error", "message": "Data fetch failed."}

    return {
        "status": "success",
        "data": {
            "page": pag_info.page,
            "pageSize": pag_info.page_size,
            "results": len(instances),
            "total": results.total,
            "instances": instances,
        },
    }


def get_model_by_id(id, model):
    try:
        id = int(id)
        instance = model.query.get(id)
    except:
        return {"status": "fail", "data": {"id": "Invalid ID"}}
    if instance == None:
        return {"status": "error", "message": "Instance with that ID does not exist."}
    return {"status": "success", "data": {"instance": model_to_dict(instance)}}


def get_model_by_2_id(id1, id2, model):
    try:
        id1 = int(id1)
        id2 = int(id2)
        instance = model.query.filter(
            and_(model.college_id == id1, model.company_id == id2)
        ).first()
    except:
        return {"status": "fail", "data": {"id": "Invalid ID"}}
    if instance == None:
        return {"status": "error", "message": "Instance with that ID does not exist."}
    return {"status": "success", "data": {"instance": model_to_dict(instance)}}


def add_search_filters(base_query, fields, val):
    # fields are the searchable columns want this to be done by the backend so the endpoint will pass in this field
    search_str = "%"
    for s in val.split():
        search_str += s + "%"
    search_args = []
    for field in fields:
        if field.type.python_type is list:
            search_args.append(func.array_to_string(field, ",").ilike(search_str))
        else:
            search_args.append(field.ilike(search_str))
    return base_query.filter(or_(*search_args))


def add_exact_filters(base_query, filters):
    query_filters = []
    for field in filters:
        if field.type.python_type is list:
            query_filters.append(field.contains(filters[field]))
        else:
            query_filters.append(field.in_(filters[field]))
    return base_query.filter(*query_filters)


def add_range_filters(base_query, filters):
    query_filters = []
    for field in filters:
        min, max = filters[field]
        if min is not None:
            query_filters.append(field >= min)
        if max is not None:
            query_filters.append(field <= max)
    return base_query.filter(and_(*query_filters))


def model_to_dict(model):
    attributes = inspect.getmembers(model, lambda a: not (inspect.isroutine(a)))

    ret_dict = {}
    inv_name_dict = {v: k for k, v in model.name_dict.items()}
    for attr in attributes:
        if attr[0] in inv_name_dict:
            ret_dict[inv_name_dict[attr[0]]] = attr[1]

    return ret_dict

def reupload_images(model: db.Model, image_field: str, folder: str, name_field: str, secondary_name_field: str = None):
    results = model.query.all()
    for instance in results:
        name = getattr(instance, name_field)
        if secondary_name_field is not None:
            name += '-' + getattr(instance, secondary_name_field)
        name = re.sub("[^\w\s-]", "", name)
        name = re.sub("\s+", "-", name)
        print(getattr(instance, name_field) + "  =>  " + name)
        url = getattr(instance, image_field)
        if url is not None and not url.find(base_url) != -1:
            picture_url = upload_url_image(folder, getattr(instance, image_field), name)
            setattr(instance, image_field, picture_url)
        else:
            print("skip")
        db.session.commit()
        # break
    return 'Done'


class Auth_Key(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.Text)
    db_perm = db.Column(db.Boolean)

    __tablename__ = "auth_key"


def check_auth_key(req_args):
    user_auth_key = 0
    try:
        user_auth_key = req_args.get("auth_key")
        if user_auth_key is None:
            raise
    except:
        return False
    db_auth_key = Auth_Key.query.filter_by(key=user_auth_key).first()
    if db_auth_key is None or not db_auth_key.db_perm:
        return False
    return True
