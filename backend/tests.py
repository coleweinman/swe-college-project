from unittest import main, TestCase
import common
from models.nearby_college_company import nearby_college_company
from models.college import College
from models.city import City
from models.company import Company
from endpoints.colleges import get_colleges
from endpoints.companies import get_companies
from endpoints.cities import get_cities
from services import app

VALID_COLLEGE_ID = 1
VALID_COMPANY_ID = 9
VALID_CITY_ID = 2


class UnitTests(TestCase):
    def setUp(self):
        pass

    # --- COLLEGES ENDPOINTS TESTS ---

    # Test no page information, required columns or distance
    def test_1(self):
        request_args = {}
        requrl = "/colleges"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["colleges"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test with page information
    def test_2(self):
        requrl = "/colleges?page=2&pageSize=30"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 2)
        self.assertEqual(ret["data"]["pageSize"], 30)
        self.assertLessEqual(ret["data"]["results"], 30)
        self.assertNotIn("distance", ret["data"]["colleges"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test required field for columns that are not null
    def test_3(self):
        requrl = "/colleges?required=satAvg,state"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["colleges"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with no max distance
    def test_4(self):
        requrl = f"/colleges?nearbyCompanyId={VALID_COMPANY_ID}"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertIn("distance", ret["data"]["colleges"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with max distance
    def test_5(self):
        requrl = f"/colleges?nearbyCompanyId={VALID_COMPANY_ID}&maxDistance=500"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertIn("distance", ret["data"]["colleges"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with max distance of zero
    def test_6(self):
        requrl = f"/colleges?nearbyCompanyId={VALID_COMPANY_ID}&maxDistance=0"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertEqual(ret["data"]["total"], 0)

    # Test getting college by id
    def test_7(self):
        ret = common.get_model_by_id(VALID_COLLEGE_ID, College)
        self.assertEqual(ret["status"], "success")

    # Test sorting, ascending
    def test_8(self):
        requrl = f"/colleges?sortField=satAvg&ascending=true"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["colleges"][1]["satAvg"], ret["data"]["colleges"][0]["satAvg"]
        )

    # Test sorting, descending
    def test_9(self):
        requrl = f"/colleges?sortField=satAvg&ascending=FALSE"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["colleges"][0]["satAvg"], ret["data"]["colleges"][1]["satAvg"]
        )

    # Test searching
    def test_10(self):
        requrl = f"/colleges?query=Ohio"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("Ohio", ret["data"]["colleges"][0]["name"])

    # Test exact filters
    def test_11(self):
        requrl = f"/colleges?cityNameFilter=Houston"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("Houston", ret["data"]["colleges"][0]["cityName"])

    # Test range filters
    def test_12(self):
        requrl = f"/colleges?satAvgMin=1450&satAvgMax=1500"
        with app.test_request_context(requrl):
            ret = get_colleges()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        for college in ret["data"]["colleges"]:
            self.assertGreaterEqual(1500, college["satAvg"])
            self.assertLessEqual(1450, college["satAvg"])

    # --- COMPANIES ENDPOINTS TESTS ---

    # Test no page information, required columns or distance
    def test_13(self):
        requrl = f"/companies"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["companies"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test with page information
    def test_14(self):
        requrl = f"/companies?page=2&pageSize=30"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 2)
        self.assertEqual(ret["data"]["pageSize"], 30)
        self.assertLessEqual(ret["data"]["results"], 30)
        self.assertNotIn("distance", ret["data"]["companies"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test required field for columns that are not null
    def test_15(self):
        requrl = f"/companies?required=annualRevenue,cityId"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["companies"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with no max distance
    def test_16(self):
        requrl = f"/companies?nearbyCollegeId={VALID_COLLEGE_ID}"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertIn("distance", ret["data"]["companies"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with max distance
    def test_17(self):
        requrl = f"/companies?nearbyCollegeId={VALID_COLLEGE_ID}&maxDistance=500"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertIn("distance", ret["data"]["companies"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test distances with max distance of zero
    def test_18(self):
        requrl = f"/companies?nearbyCollegeId={VALID_COLLEGE_ID}&maxDistance=0"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertEqual(ret["data"]["total"], 0)

    # Test getting company by id
    def test_19(self):
        ret = common.get_model_by_id(VALID_COMPANY_ID, Company)
        self.assertEqual(ret["status"], "success")

    # Test sorting, ascending
    def test_20(self):
        requrl = f"/companies?sortField=annualRevenue&ascending=true"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["companies"][1]["annualRevenue"],
            ret["data"]["companies"][0]["annualRevenue"],
        )

    # Test sorting, descending
    def test_21(self):
        requrl = f"/companies?sortField=annualRevenue&ascending=FALSE"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["companies"][0]["annualRevenue"],
            ret["data"]["companies"][1]["annualRevenue"],
        )

    # Test searching
    def test_22(self):
        requrl = f"/companies?query=Amazon"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("Amazon", ret["data"]["companies"][0]["name"])

    # Test exact filters
    def test_23(self):
        requrl = f"/companies?cityNameFilter=Seattle"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("Seattle", ret["data"]["companies"][0]["cityName"])

    # Test range filters
    def test_24(self):
        requrl = f"/companies?employeesMin=5000&employeesMax=6000"
        with app.test_request_context(requrl):
            ret = get_companies()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        for company in ret["data"]["companies"]:
            self.assertGreaterEqual(6000, company["employees"])
            self.assertLessEqual(5000, company["employees"])

    # --- CITIES ENDPOINTS TESTS ---

    # Test no page information, required columns or distance
    def test_25(self):
        requrl = f"/cities"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["cities"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test with page information
    def test_26(self):
        requrl = f"/cities?page=2&pageSize=30"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 2)
        self.assertEqual(ret["data"]["pageSize"], 30)
        self.assertLessEqual(ret["data"]["results"], 30)
        self.assertNotIn("distance", ret["data"]["cities"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test required field for columns that are not null
    def test_27(self):
        requrl = f"/cities?required=name,state"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertLessEqual(ret["data"]["results"], 15)
        self.assertNotIn("distance", ret["data"]["cities"][0])
        self.assertGreater(ret["data"]["total"], 0)

    # Test getting college by id
    def test_28(self):
        ret = common.get_model_by_id(VALID_CITY_ID, City)
        self.assertEqual(ret["status"], "success")

    # Test sorting, ascending
    def test_29(self):
        requrl = f"/cities?sortField=population&ascending=true"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["cities"][1]["population"],
            ret["data"]["cities"][0]["population"],
        )

    # Test sorting, descending
    def test_30(self):
        requrl = f"/colleges?sortField=population&ascending=FALSE"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertGreaterEqual(
            ret["data"]["cities"][0]["population"],
            ret["data"]["cities"][1]["population"],
        )

    # Test searching
    def test_31(self):
        requrl = f"/cities?query=Houston"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("Houston", ret["data"]["cities"][0]["name"])

    # Test exact filters
    def test_32(self):
        requrl = f"/cities?stateFilter=TX"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        self.assertEqual(ret["data"]["pageSize"], 15)
        self.assertIn("TX", ret["data"]["cities"][0]["state"])

    # Test range filters
    def test_33(self):
        requrl = f"/cities?populationMin=7000&populationMax=8000"
        with app.test_request_context(requrl):
            ret = get_cities()
        self.assertEqual(ret["status"], "success")
        self.assertEqual(ret["data"]["page"], 1)
        for city in ret["data"]["cities"]:
            self.assertGreaterEqual(8000, city["population"])
            self.assertLessEqual(7000, city["population"])


if __name__ == "__main__":
    main()
