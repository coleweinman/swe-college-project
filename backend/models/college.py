from services import db

ownership_enum = ["", "public", "private non-profit", "private for-profit"]
degrees_enum = ["", "certificate", "associate", "bachelor", ""]
level_enum = ["", "4-year", "2-year", ""]
testreq_enum = {1: "required", 5: "considered but not required"}
states_enum = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming",
}


class College(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    city_id = db.Column(db.Integer)
    city_name = db.Column(db.Text)
    state = db.Column(db.Text)
    type_control = db.Column(db.Text)
    type_degree = db.Column(db.Text)
    description = db.Column(db.Text)
    home_url = db.Column(db.Text)
    picture_url = db.Column(db.Text)
    thumbnail_url = db.Column(db.Text)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    religion_aff = db.Column(db.Text)
    admission_rate = db.Column(db.Float)
    sat_avg = db.Column(db.Integer)
    sat_mid_math = db.Column(db.Integer)
    sat_mid_read = db.Column(db.Integer)
    enrollment = db.Column(db.Integer)
    avg_cost = db.Column(db.Integer)
    tuition_in = db.Column(db.Integer)
    tuition_out = db.Column(db.Integer)
    test_score_req = db.Column(db.Text)
    level = db.Column(db.Text)

    name_dict = {
        "id": "id",
        "name": "name",
        "cityId": "city_id",
        "cityName": "city_name",
        "state": "state",
        "typeControl": "type_control",
        "typeDegree": "type_degree",
        "description": "description",
        "homeUrl": "home_url",
        "pictureUrl": "picture_url",
        "thumbnailUrl": "thumbnail_url",
        "latitude": "latitude",
        "longitude": "longitude",
        "religiousAffiliation": "religion_aff",
        "admissionRate": "admission_rate",
        "satAvg": "sat_avg",
        "satMidMath": "sat_mid_math",
        "satMidRead": "sat_mid_read",
        "enrollmentSize": "enrollment",
        "avgCost": "avg_cost",
        "tuitionInState": "tuition_in",
        "tuitionOutOfState": "tuition_out",
        "testScoreReq": "test_score_req",
        "level": "level",
    }

    def fromAPIVals(self, vals):
        self.name = vals["school.name"]
        self.city_name = vals["school.city"]
        self.state = vals["school.state"]
        self.type_control = ownership_enum[vals["school.ownership"]]
        self.type_degree = degrees_enum[vals["school.degrees_awarded.predominant"]]
        self.home_url = vals["school.school_url"]
        self.latitude = vals["location.lat"]
        self.longitude = vals["location.lon"]
        # if vals['school.religious_affiliation'] is not None:
        #     self.religion_aff = vals['school.religious_affiliation']
        self.admission_rate = vals["latest.admissions.admission_rate.overall"]
        self.sat_avg = vals["latest.admissions.sat_scores.average.overall"]
        self.sat_mid_math = vals["latest.admissions.sat_scores.midpoint.math"]
        self.sat_mid_read = vals[
            "latest.admissions.sat_scores.midpoint.critical_reading"
        ]
        self.enrollment = vals["latest.student.size"]
        self.avg_cost = vals["latest.cost.attendance.academic_year"]
        self.tuition_in = vals["latest.cost.tuition.in_state"]
        self.tuition_out = vals["latest.cost.tuition.out_of_state"]
        self.test_score_req = testreq_enum[vals["latest.admissions.test_requirements"]]
        self.level = level_enum[vals["school.institutional_characteristics.level"]]

        self.api_vals = vals
