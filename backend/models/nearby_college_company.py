from services import db


class nearby_college_company(db.Model):
    college_id = db.Column(db.Integer, db.ForeignKey("college.id"), primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey("company.id"), primary_key=True)
    distance = db.Column(db.Float)

    def to_dict(self):
        return {
            "college_id": self.college_id,
            "company_id": self.company_id,
            "distance": self.distance,
        }
