from services import db
from sqlalchemy.dialects.postgresql import ARRAY


class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    state = db.Column(db.Text)

    population = db.Column(db.Integer)
    walk_score_url = db.Column(db.Text)
    budget = db.Column(db.Integer)
    safety = db.Column(db.Integer)
    google_events_url = db.Column(db.Text)
    known_for = db.Column(ARRAY(db.String()))

    elevation = db.Column(db.Integer)
    timezone = db.Column(db.Text)

    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    area = db.Column(db.Float)
    population_density = db.Column(db.Float)

    covid_score = db.Column(db.Float)
    average_rating = db.Column(db.Float)

    image_url = db.Column(db.Text)
    description = db.Column(db.Text)

    name_dict = {
        "id": "id",
        "name": "name",
        "state": "state",
        "population": "population",
        "latitude": "latitude",
        "longitude": "longitude",
        "walkScoreUrl": "walk_score_url",
        "budget": "budget",
        "safety": "safety",
        "googleEventsUrl": "google_events_url",
        "knownFor": "known_for",
        "elevation": "elevation",
        "timezone": "timezone",
        "area": "area",
        "populationDensity": "population_density",
        "covidScore": "covid_score",
        "averageRating": "average_rating",
        "imageUrl": "image_url",
        "description": "description",
    }
