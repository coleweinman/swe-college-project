from services import db
from sqlalchemy.dialects.postgresql import ARRAY


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String)
    city_name = db.Column(db.String())
    state = db.Column(db.String())
    city_id = db.Column(db.Integer, db.ForeignKey("city.id"), nullable=True)
    url = db.Column(db.String())
    domain = db.Column(db.String())
    logo_url = db.Column(db.String())
    industries = db.Column(ARRAY(db.String()))
    technologies = db.Column(ARRAY(db.String()))
    public = db.Column(db.Boolean)
    ticker = db.Column(db.String())
    employees = db.Column(db.Integer)
    alexa_us_rank = db.Column(db.Integer)
    alexa_global_rank = db.Column(db.Integer)
    annual_revenue = db.Column(db.Integer)
    sector = db.Column(db.String())
    location = db.Column(db.String())
    legal_name = db.Column(db.String())
    founded = db.Column(db.Integer)
    twitter_handle = db.Column(db.String)
    facebook_handle = db.Column(db.String)
    linkedin_handle = db.Column(db.String)
    instagram_handle = db.Column(db.String)
    youtube_handle = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    summary = db.Column(db.String)
    favicon_url = db.Column(db.String)
    icon_url = db.Column(db.String)


    name_dict = {
        "id": "id",
        "name": "name",
        "description": "description",
        "cityName": "city_name",
        "state": "state",
        "cityId": "city_id",
        "url": "url",
        "domain": "domain",
        "logoURL": "logo_url",
        "industries": "industries",
        "technologies": "technologies",
        "public": "public",
        "ticker": "ticker",
        "employees": "employees",
        "alexaUsRank": "alexa_us_rank",
        "alexaGlobalRank": "alexa_global_rank",
        "annualRevenue": "annual_revenue",
        "sector": "sector",
        "location": "location",
        "legalName": "legal_name",
        "founded": "founded",
        "twitterHandle": "twitter_handle",
        "facebookHandle": "facebook_handle",
        "linkedinHandle": "linkedin_handle",
        "instagramHandle": "instagram_handle",
        "latitude": "latitude",
        "longitude": "longitude",
        "faviconUrl": "favicon_url",
        "iconUrl": "icon_url",
        "summary": "summary",
    }
