from flask import Blueprint, render_template, abort, jsonify, request
from models.nearby_college_company import nearby_college_company
from models.college import College
from services import db
from models.city import City
import common
import requests
import os
import math

colleges = Blueprint("colleges", __name__)


# --- DATA RETRIEVAL ENDPOINTS ---


@colleges.route("/colleges")
def get_colleges():
    exact_filter_fields = [
        College.state,
        College.city_name,
        College.type_control,
        College.type_degree,
        College.test_score_req,
        College.level,
    ]
    range_filter_fields = [
        College.admission_rate,
        College.sat_avg,
        College.sat_mid_math,
        College.sat_mid_read,
        College.enrollment,
        College.avg_cost,
        College.tuition_in,
        College.tuition_out,
    ]
    search_fields = [
        College.name,
        College.state,
        College.city_name,
    ]
    sort_fields = [
        College.name,
        College.admission_rate,
        College.sat_avg,
        College.sat_mid_math,
        College.sat_mid_read,
        College.enrollment,
        College.tuition_in,
        College.tuition_out,
        College.avg_cost,
    ]

    # Get pagination info
    try:
        pag_info = common.parse_models_pagination_params(request.args)
        nearby_company_id = common.parse_request_param(
            request.args, "nearbyCompanyId", int
        )
        max_distance = common.parse_request_param(request.args, "maxDistance", int)
        required_fields = common.parse_request_param(
            request.args, "required", str, list=True
        )
        query_field = common.parse_request_param(request.args, "query", str)
        exact_filters = common.parse_exact_filters(
            request.args, College, exact_filter_fields
        )
        range_filters = common.parse_range_filters(
            request.args, College, range_filter_fields
        )
        sort_logic = common.parse_sort_fields(request.args, College, sort_fields, College.enrollment)
    except Exception as e:
        return {"status": "fail", "data": e.args}

    # Special query with distances from nearby college
    if nearby_company_id is not None:
        ret = common.nearby_query(
            College,
            pag_info,
            nearby_model_id_field=nearby_college_company.college_id,
            nearby_id_field=nearby_college_company.company_id,
            nearby_id=nearby_company_id,
            max_distance=max_distance,
        )
        if ret["status"] == "success":
            ret["data"]["colleges"] = ret["data"].pop("instances")
        return ret

    base_query = College.query
    base_query = common.add_required_filters(base_query, College, required_fields)

    if query_field is not None:
        base_query = common.add_search_filters(base_query, search_fields, query_field)
    if len(exact_filters) > 0:
        base_query = common.add_exact_filters(base_query, exact_filters)
    if len(range_filters) > 0:
        base_query = common.add_range_filters(base_query, range_filters)

    # Apply Sorting
    if sort_logic is None:
        base_query = base_query.order_by(College.enrollment.desc())
    else:
        base_query = base_query.order_by(sort_logic)

    ret = common.get_model_pages(base_query, pag_info)

    if ret["status"] == "success":
        ret["data"]["colleges"] = ret["data"].pop("instances")
    return ret


@colleges.route("/colleges/<id>")
def get_college_by_id(id):
    ret = common.get_model_by_id(id, College)

    if ret["status"] == "success":
        ret["data"]["college"] = ret["data"].pop("instance")

    return ret


# --- SCRAPING ENDPONTS ---


@colleges.route("/populateCollegeGeneralData")
def populate_general_data():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"

    colleges = []
    req_url = "https://api.data.gov/ed/collegescorecard/v1/schools.json?fields=school.name,school.state,school.ownership,school.degrees_awarded.predominant,school.school_url,location.lat,location.lon,school.religious_aff,latest.admissions.admission_rate.overall,latest.admissions.sat_scores.average.overall,latest.admissions.sat_scores.midpoint.math,latest.admissions.sat_scores.midpoint.critical_reading,latest.student.size,latest.cost.attendance.academic_year,latest.cost.tuition.in_state,latest.cost.tuition.out_of_state,latest.admissions.test_requirements,school.institutional_characteristics.level,school.city&school.degrees_awarded.predominant=1,2,3&school.ownership__not=null&latest.admissions.admission_rate.overall__not=null&latest.admissions.test_requirements__not=null&latest.admissions.sat_scores.average.overall__not=null&latest.admissions.sat_scores.midpoint.math__not=null&latest.admissions.sat_scores.midpoint.critical_reading__not=null&latest.student.size__not=null&latest.cost.tuition.in_state__not=null&latest.cost.tuition.out_of_state__not=null&school.school_url__not=null&school.city__not=null&school.main_campus=1&per_page=100"
    req_url += "&api_key=" + os.environ.get("COLLEGE_SC_KEY")
    response = requests.get(req_url + "&page=0")

    # Retrieve college information from APIs
    # Results split into pages, so need to loop over all pages
    total_results = response.json()["metadata"]["total"]
    per_page = response.json()["metadata"]["per_page"]
    for p in range(math.ceil(total_results / per_page)):
        print("API Page: ", p)
        response = requests.get(req_url + "&page=" + str(p))
        for result in response.json()["results"]:
            college = College()
            college.fromAPIVals(result)
            colleges.append(college)

    # Add colleges to college table in database
    for i, college in enumerate(colleges):
        if i % 100 == 0:
            print("Adding colleges", i, "-", (i + 100), "to table")

        # If college already in table, modify existing entry. Otherwise add new
        q_college = College.query.filter_by(name=college.name).first()
        if q_college is None:
            db.session.add(college)
        else:
            q_college.fromAPIVals(college.api_vals)
            q_college.city_id = college.city_id
            q_college.description = college.description

    db.session.commit()
    print("Request Complete")

    return "success"


@colleges.route("/populateCollegeCities")
def populate_college_cities():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"

    colleges = College.query.all()

    # Add colleges to college table in database
    for i, college in enumerate(colleges):
        if i % 100 == 0:
            print("Adding colleges", i, "-", (i + 100), "to table")

        # Check if city entry exists in city table, if not then add it
        q_city = City.query.filter_by(
            name=college.city_name, state=college.state
        ).first()
        city_id = None
        if q_city is None:
            city = City()
            city.name = college.city_name
            city.state = college.state
            db.session.add(city)
            city_id = (
                City.query.filter_by(name=college.city_name, state=college.state)
                .first()
                .id
            )
        else:
            city_id = q_city.id
        college.city_id = city_id

    db.session.commit()
    print("Request Complete")

    return "success"


@colleges.route("/populateCollegeDescriptions")
def populate_college_descriptions():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"

    colleges = College.query.all()
    wiki_search_url = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsort=relevance"
    wiki_desc_url = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts%7Cpageimages&redirects=1&exintro=1&explaintext=1&exsectionformat=plain&piprop=original"

    for college in colleges:
        # Get description of college from wikipedia
        # First perform a wikipedia search to find title of page that is
        # most likely to be page of the college
        wiki_search_req = wiki_search_url + "&srsearch=" + college.name
        wiki_search_resp = requests.get(wiki_search_req)
        try:
            wiki_title = wiki_search_resp.json()["query"]["search"][0]["title"]
        except:
            print("WARNING: Issues with search query response from", college.name)
        else:
            # Next, use the found title to retrieve the actual description
            wiki_desc_req = wiki_desc_url + "&titles=" + wiki_title
            wiki_desc_resp = requests.get(wiki_desc_req)
            wiki_desc = None
            try:
                wiki_desc = list(wiki_desc_resp.json()["query"]["pages"].values())[0][
                    "extract"
                ]
            except:
                print(
                    "WARNING: Issues with description query response from", college.name
                )
            else:
                # Check if found page is actually that of a college
                check_1 = (
                    wiki_desc.find("University") > 200
                    or wiki_desc.find("University") < 0
                )
                check_2 = (
                    wiki_desc.find("university") > 200
                    or wiki_desc.find("university") < 0
                )
                check_3 = (
                    wiki_desc.find("college") > 200 or wiki_desc.find("college") < 0
                )
                check_4 = (
                    wiki_desc.find("College") > 200 or wiki_desc.find("College") < 0
                )
                if check_1 and check_2 and check_3 and check_4:
                    print(
                        "WARNING: Possible incorrect wikipedia page detected for",
                        college.name,
                    )
                else:
                    college.description = wiki_desc

    db.session.commit()
    print("Request Complete")

    return "success"

@colleges.route("/reuploadCollegeImages")
def reupload_images():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"
    common.reupload_images(College, "picture_url", "college-pictures", "name")
    common.reupload_images(College, "thumbnail_url", "college-thumbnails", "name")
    return 'Done'

@colleges.route("/populateCollegeImages")
def populate_images():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"

    colleges = College.query.filter_by(picture_url=None).all()
    req_url = "https://api.bing.microsoft.com/v7.0/images/search"
    api_key = os.environ.get("BING_SEARCH_KEY")

    for college in colleges:
        headers = {"Ocp-Apim-Subscription-Key": api_key}
        query_str = college.name
        if college.name.find("campus") < 0:
            query_str += " campus"
        params = {"q": query_str, "imageType": "photo"}
        resp = requests.get(req_url, headers=headers, params=params)

        img_url = None
        thumb_url = None
        try:
            img_url = resp.json()["value"][0]["contentUrl"]
            thumb_url = resp.json()["value"][0]["thumbnailUrl"]
        except:
            print("Issue with getting image for", college.name)
            print(resp.json())
        else:
            college.picture_url = img_url
            college.thumbnail_url = thumb_url

    db.session.commit()
    print("Request Complete")
    return "success with images"
