import os

from flask import Blueprint, render_template, abort, request
from jinja2 import TemplateNotFound
from services import db
from sqlalchemy.dialects.postgresql import ARRAY
from models.college import College
from models.nearby_college_company import nearby_college_company
from models.company import Company

import requests
import common
from sqlalchemy.sql import text
import geopy.distance

distances = Blueprint("distances", __name__)


@distances.route("/populateDistance")
def populate():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"

    companies = Company.query.all()
    colleges = College.query.all()

    for company in companies:
        for college in colleges:
            d = geopy.distance.distance(
                (company.latitude, company.longitude),
                (college.latitude, college.longitude),
            ).miles
            distance = nearby_college_company(
                company_id=company.id, college_id=college.id, distance=d
            )
            db.session.add(distance)
    db.session.commit()
    return "populated table"
