import os

from flask import Blueprint, render_template, abort, request
from models.nearby_college_company import nearby_college_company
from services import db, upload_url_image
from models.city import City
from models.company import Company

import requests
import common
import json
import math
companies = Blueprint("companies", __name__)


@companies.route("/companies")
def get_companies():
    range_filter_fields = [Company.annual_revenue, Company.employees]
    search_fields = [Company.name, Company.state, Company.city_name, Company.sector]
    sort_fields = [
        Company.name,
        Company.employees,
        Company.alexa_global_rank,
        Company.alexa_us_rank,
        Company.annual_revenue,
        Company.founded,
    ]
    exact_filter_fields = [
        Company.state,
        Company.city_name,
        Company.public,
        Company.industries,
        Company.sector,
    ]

    # Get pagination info
    try:
        pag_info = common.parse_models_pagination_params(request.args)
        nearby_college_id = common.parse_request_param(
            request.args, "nearbyCollegeId", int
        )
        max_distance = common.parse_request_param(request.args, "maxDistance", int)
        required_fields = common.parse_request_param(
            request.args, "required", str, list=True
        )
        query_field = common.parse_request_param(request.args, "query", str)
        exact_filters = common.parse_exact_filters(
            request.args, Company, exact_filter_fields
        )
        range_filters = common.parse_range_filters(
            request.args, Company, range_filter_fields
        )
        sort_logic = common.parse_sort_fields(request.args, Company, sort_fields, Company.annual_revenue)
    except Exception as e:
        return {"status": "fail", "data": e.args}

    # Special query with distances from nearby college
    if nearby_college_id is not None:
        ret = common.nearby_query(
            Company,
            pag_info,
            nearby_model_id_field=nearby_college_company.company_id,
            nearby_id_field=nearby_college_company.college_id,
            nearby_id=nearby_college_id,
            max_distance=max_distance,
        )
        if ret["status"] == "success":
            ret["data"]["companies"] = ret["data"].pop("instances")
        return ret

    base_query = Company.query
    base_query = common.add_required_filters(base_query, Company, required_fields)
    if query_field is not None:
        base_query = common.add_search_filters(base_query, search_fields, query_field)
    if len(exact_filters) > 0:
        base_query = common.add_exact_filters(base_query, exact_filters)
    if len(range_filters) > 0:
        base_query = common.add_range_filters(base_query, range_filters)

    # Apply Sorting
    if sort_logic is None:
        base_query = base_query.order_by(Company.annual_revenue.desc())
    else:
        base_query = base_query.order_by(sort_logic)

    ret = common.get_model_pages(base_query, pag_info)

    if ret["status"] == "success":
        ret["data"]["companies"] = ret["data"].pop("instances")
    return ret


@companies.route("/companies/<id>")
def get_company_by_id(id):
    ret = common.get_model_by_id(id, Company)

    if ret["status"] == "success":
        ret["data"]["company"] = ret["data"].pop("instance")

    return ret


@companies.route("/addCompanyByDomain")
def add_company_by_domain():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"
    if "domain" not in request.args:
        return "Missing domain parameter"
    domain = request.args["domain"]
    if company_exists(domain):
        return domain + " is already in the database"
    else:
        result = add_company(domain)
        if result:
            return domain + " added successfully"
        else:
            return domain + " failed to be added"


@companies.route("/addCompaniesByDomain")
def add_companies_by_domain():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"
    if "domains" not in request.args:
        return "Missing domains parameter"
    successful = []
    failed = []
    exists = []
    domains = request.args["domains"]
    domains_list = domains.split(",")
    for domain in domains_list:
        if company_exists(domain):
            exists.append(domain)
        else:
            result = add_company(domain)
            if result:
                successful.append(domain)
            else:
                failed.append(domain)
    return_html = ""
    return_html += "<p>Successful Domains:"
    for domain in successful:
        return_html += "<br>" + domain
    return_html += "</p>"
    return_html += "<p>Failed Domains:"
    for domain in failed:
        return_html += "<br>" + domain
    return_html += "</p>"
    return_html += "<p>Existing Domains:"
    for domain in exists:
        return_html += "<br>" + domain
    return_html += "</p>"
    return return_html

def get_city_id(city_name: str, state_name: str):
    city = City.query.filter_by(name=city_name, state=state_name).first()

    # Add city if it doesn't exist
    if city == None:
        city = City(
            name=city_name,
            state=state_name,
            known_for=[],
            population=0,
            description=""
        )
        db.session.add(city)
        return City.query.filter_by(name=city_name, state=state_name).first().id
    else:
        return city.id


def company_exists(domain: str):
    results = Company.query.filter(Company.domain == domain).all()
    return len(results) > 0


def get_cords(address: str):
    try:
        response = requests.get(
            "http://api.positionstack.com/v1/forward",
            params={"access_key": os.environ.get("PS_AUTH_HEADER"), "query": address},
        )
        json = response.json()
        data = json["data"][0]
    except:
        return {"latitude": None, "longitude": None}
    return {"latitude": data["latitude"], "longitude": data["longitude"]}

def add_company(domain: str) -> bool:
    try:
        response = requests.get(
            "https://company.bigpicture.io/v1/companies/find",
            params={"domain": domain},
            headers={"Authorization": os.environ.get("BP_AUTH_HEADER")},
        )
        data = response.json()
        if response.status_code == 202:
            print(domain + "ASYNC CREATED")
            return False
        # Stop if not in US
        if data["geo"]["countryCode"] != "US":
            print(domain + " NOT IN US")
            return False
        brand_response = requests.get(
            "https://api.brandfetch.io/v2/brands/" + domain,
            headers={"Authorization": "Bearer " + os.environ.get("BF_AUTH_HEADER")}
        )
        brand_response_data = brand_response.json()

        # Retrieve social media handles, except linkedin
        socials = {
            "twitter": None,
            "instagram": None,
            "facebook": None,
            "youtube": None,
            "linkedin": data["linkedin"]["handle"]
        }
        for link in brand_response_data["links"]:
            if link["name"] in socials:
                url = link["url"]
                handle = url[url.rindex("/") + 1 :]
                socials[link["name"]] = handle

        # Retrieve images and reupload them to get fast link
        images = {
            "logo": None,
            "icon": None,
            "favicon": "https://icon.horse/icon/" + domain
        }
        for logo in brand_response_data["logos"]:
            if logo["type"] in images:
                images[logo["type"]] = logo["formats"][0]["src"]
        for img_name in images:
            if images[img_name] is not None:
                images[img_name] = upload_url_image(img_name + "s", images[img_name], domain)

        # Retrieve longitude and latitude
        cords = get_cords(data["location"])
        if data["name"] is None:
            print(domain + " NO NAME")
            return False
        # Construct company model
        company = Company(
            name=data["name"],
            description=data["description"],
            city_name=data["geo"]["city"],
            city_id=get_city_id(data["geo"]["city"], data["geo"]["stateCode"]),
            state=data["geo"]["stateCode"],
            url=data["url"],
            domain=domain,
            logo_url=images["logo"],
            industries=data["tags"],
            technologies=data["tech"],
            public=(
                data["type"] == None
                if None
                else data["type"] == "public"
                if True
                else False
            ),
            ticker=data["ticker"],
            employees=data["metrics"]["employees"],
            alexa_us_rank=data["metrics"]["alexaUsRank"],
            alexa_global_rank=data["metrics"]["alexaGlobalRank"],
            annual_revenue=data["metrics"]["annualRevenue"] or 0,
            sector=data["category"]["sector"],
            location=data["location"],
            legal_name=data["legalName"],
            founded=data["foundedYear"],
            twitter_handle=socials["twitter"],
            facebook_handle=socials["facebook"],
            linkedin_handle=data["linkedin"]["handle"],
            instagram_handle=socials["instagram"],
            youtube_handle=socials['youtube'],
            latitude=cords["latitude"],
            longitude=cords["longitude"],
            favicon_url=images["favicon"],
            icon_url=images["icon"],
            summary=brand_response_data["description"],
        )
        db.session.add(company)
        db.session.commit()
    except Exception as e:
        print(domain + " ERROR")
        print(e)
        return False
    return True
