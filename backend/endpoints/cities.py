from flask import Blueprint, render_template, abort, request
from jinja2 import TemplateNotFound
from common import model_to_dict
from models.company import Company
from models.college import College
from models.city import City

import json
import common

cities = Blueprint("cities", __name__)


@cities.route("/cities")
def get_cities():
    exact_filter_fields = [City.state, City.timezone, City.known_for]
    range_filter_fields = [
        City.population,
        City.budget,
        City.safety,
        City.elevation,
        City.area,
        City.population_density,
        City.covid_score,
        City.average_rating,
    ]
    search_fields = [City.name, City.state, City.known_for]
    sort_fields = [
        City.name,
        City.population,
        City.budget,
        City.safety,
        City.elevation,
        City.area,
        City.population_density,
        City.covid_score,
        City.average_rating,
    ]

    # Get pagination info
    try:
        pag_info = common.parse_models_pagination_params(request.args)
        required_fields = common.parse_request_param(
            request.args, "required", str, list=True
        )
        query_field = common.parse_request_param(request.args, "query", str)
        exact_filters = common.parse_exact_filters(
            request.args, City, exact_filter_fields
        )
        range_filters = common.parse_range_filters(
            request.args, City, range_filter_fields
        )
        sort_logic = common.parse_sort_fields(request.args, City, sort_fields, City.population)
    except Exception as e:
        return {"status": "fail", "data": e.args}

    base_query = City.query
    base_query = common.add_required_filters(base_query, City, required_fields)
    if query_field is not None:
        base_query = common.add_search_filters(base_query, search_fields, query_field)
    if len(exact_filters) > 0:
        base_query = common.add_exact_filters(base_query, exact_filters)
    if len(range_filters) > 0:
        base_query = common.add_range_filters(base_query, range_filters)

    # Apply Sorting
    if sort_logic is None:
        base_query = base_query.order_by(City.population.desc())
    else:
        base_query = base_query.order_by(sort_logic)

    ret = common.get_model_pages(base_query, pag_info)

    if ret["status"] == "success":
        ret["data"]["cities"] = ret["data"].pop("instances")
    return ret


@cities.route("/cities/<id>")
def get_city_by_id(id):
    ret = common.get_model_by_id(id, City)

    if ret["status"] == "success":
        ret["data"]["city"] = ret["data"].pop("instance")

    city = ret["data"]["city"]
    colleges = College.query.filter_by(city_id=city["id"]).all()
    companies = Company.query.filter_by(city_id=city["id"]).all()
    city["nearbyColleges"] = []
    city["nearbyCompanies"] = []
    for c in colleges:
        city["nearbyColleges"].append(model_to_dict(c))
    for c in companies:
        city["nearbyCompanies"].append(model_to_dict(c))

    return ret

@cities.route("/reuploadCityImages")
def reupload_images():
    if not common.check_auth_key(request.args):
        return "Error: Bad authentication"
    common.reupload_images(City, "image_url", "city-images", "name", secondary_name_field="state")
    return 'Done'

@cities.route("/populateCities")
def populate():

    return "request sent"


@cities.route("/cities/filter", methods=["POST"])
def filter():
    input = request.get_json()
    ret = common.filter(City, input)
    if ret["status"] == "success":
        ret["data"]["cities"] = ret["data"].pop("instance")
    return ret


@cities.route("/cities/search")
def search():
    # the only argument should be the value
    input = request.args["val"]
    searchable_fields = ["name", "state"]
    ret = common.search(City, searchable_fields, input)
    if ret["status"] == "success":
        ret["data"]["cities"] = ret["data"].pop("instance")
    return ret


@cities.route("/getCities")
def base():
    return "city"
    # return City.query.all()[0].toDict()
