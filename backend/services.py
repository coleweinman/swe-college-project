import os
import traceback
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_cors import CORS
from google.cloud import storage
import requests

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DB_URI")
db = SQLAlchemy(app)
CORS(app)


def init_db(app):
    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DB_URI")
    return SQLAlchemy(app)


db = init_db(app)


bucket_name = "univercity-images"
base_url = "https://storage.googleapis.com/"


def upload_url_image(folder, img_url, img_name):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(folder + "/" + img_name)
    try:
        response = requests.get(img_url, stream=True, timeout=10)
        content_type = response.headers['content-type']
        if content_type.find("image") == -1:
            return img_url
        blob.upload_from_string(response.content, content_type=content_type)
        print("Uploaded " + img_name)
        return base_url + bucket_name + "/" + folder + "/" + img_name
    except Exception:
        traceback.print_exc()
        return img_url

