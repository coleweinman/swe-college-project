import os

from services import app
from endpoints.companies import companies
from endpoints.cities import cities
from endpoints.colleges import colleges
from endpoints.distances import distances

app.register_blueprint(companies)
app.register_blueprint(cities)
app.register_blueprint(colleges)
app.register_blueprint(distances)


@app.route("/")
def hello_world():
    return "UniverCity Backend"


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
